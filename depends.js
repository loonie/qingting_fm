// export namespace
window["WebappTemplate"] = window["WebappTemplate"] || {};

window["WebappTemplate"].IpAddress = "http://app.qcast.cn/web_app/template/r26600/";
WebappTemplate.JavascriptVersion = "0.0.6";
WebappTemplate.Depends = [
        "./js/template.js",
        "./js/data/main.js",
        "./js/main_page/scene_main.js",
        "./js/main_page/block_radio_oper.js",
        "./js/main_page/block_kinds.js",
        "./js/main_page/block_radios.js",
        "./js/main_page/block_shows.js",
        "./js/data/audio.js"
    ];
