WebappTemplate.BlockRadioOper= (function(_super) {
    __extends(BlockRadioOper, _super);

    var CONST_ITEM_WIDTH = 84;
    var CONST_ITEM_HEIGHT = 130;
    // var CONST_WIDTH_GAP = 6;
    var CONST_WIDTH_GAP = 38;
    var CONST_HEIGHT_GAP = 0;
    var CONST_SCALE = 1;

    function BlockRadioOper(activity, block_root_view,template) {
        BlockRadioOper.__super__.constructor.call(this, "BlockRadioOper");

        this._HostActivity = activity;
        this._Template = template;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._PageManager = this._HostActivity._PageManager;
        this._PageUtils = this._HostActivity._PageUtils;
        this._DataMain = template._DataMain;
        this._IsPress = false;

    }

    BlockRadioOper.prototype.OnKeyDown = function(ev) {
        var key_used = BlockRadioOper.__super__.OnKeyDown.call(this, ev);
        if(!key_used){
            var scene = this.GetParentPage();
            var id = this.GetMetroFocusId();
            switch(ev.keyCode){
                case 37:
                    //left
                    if(!scene.ViewIsHide(scene._KindsClipView)){
                        if(typeof scene._RadiosBlock !="undefined" && scene._RadiosBlock.data!= null && scene._RadiosBlock.data.length>0){
                            scene._RadiosBlock.Focus();
                            this.RequestFocus(id);
                        }
                    }
                    break;
                case 39:
                    //right
                     if(!scene.ViewIsHide(scene._ShowsClipView)){
                        if(typeof scene._RadioShowsBlock  !="undefined" && scene._RadioShowsBlock.data!= null && scene._RadioShowsBlock.data.length>0){
                            scene._RadioShowsBlock.Focus();
                            this.RequestFocus(id);
                        }
                    }
                    break;
            }
        }
        return false;
    };

    BlockRadioOper.prototype._CreateBlock = function (){
        this._BuildMetro()
    };

    BlockRadioOper.prototype._BuildMetro = function() {
        this.data = [
            {
                title:"换台",
                image:"./images/normal-huantai.png",
                focus_image:"./images/focus-huantai.png"
            },
            {
                title:"收藏",
                image:"./images/normal-shoucang.png",
                focus_image:"./images/focus-shoucang.png"
            },
            {
                title:"节目单",
                image:"./images/normal-jiemubiao.png",
                focus_image:"./images/focus-jiemubiao.png"
            }
        ]
        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };

        var metro_setting = {
            // margin:{"right":20,"left":20},
            direction:"horizontal",
            lineMax: 1,
            visibleLength: 3,
            //visibleRect:new Forge.RectArea(0, 0, 1240, CONST_ITEM_HEIGHT),
            count: this.data.length,
            slideStyle:"seamless"
        };

        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(255,255,255,0)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(0, 0);
        var focus_setting = {
            useClippedFocusFrame:true,
            focusFrame:focus_frame,
            duration:250
            
        };

        var _this = this;
        var callbacks = {
            onDraw:function() { return _this._DrawItem.apply(_this, arguments); },
            onClick:function() { return _this._Click.apply(_this, arguments); },
            onDrawFocus:function() { return _this._DrawFocusItem.apply(_this, arguments) },
            onDrawBluring:null,
            // onDrawBluring:function() { return _this._DrawBlurItem.apply(_this, arguments) },
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };

    BlockRadioOper.prototype._DrawItemView = function(id, container_view,is_focus){
        var item_date = this.data[id];
        var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(26,undefined,"center","middle",this._Template.TextColor,new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, CONST_ITEM_HEIGHT),CONST_ITEM_HEIGHT),item_date.title);
        container_view.AddView(text_viewex.GetLayoutView(), {x:0, y:0});
    };

    BlockRadioOper.prototype._DrawItem = function(id, container_view) {
        var item_date = this.data[id];

        var child_view = new Forge.LayoutView();
        var scene = this._Template._MainScene;


        if(id == 1 && scene.IsCollection(scene._CurrRadioData)){
            var image_url = "./images/shoucang.png";
        }else{
            var image_url = item_date.image
        }

        var image_texture = this._TextureManager.GetImage2(image_url    ,false,{width:84,height:85},"RGBA_8888");
        var image_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(image_texture,null,null,true));
        child_view.AddView(image_view,{x:0,y:0,width:84,height:85});

        var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(28,undefined,"center","middle","#c4c4c4",new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, 30),30),item_date.title);
        child_view.AddView(text_viewex.GetLayoutView(), {x:0, y:78});

        // Add to metro
        container_view.AddView(child_view, new LayoutParams({
            x:0,
            y:0,
            width: CONST_ITEM_WIDTH,
            height: CONST_ITEM_HEIGHT
        }));
    
    };

    BlockRadioOper.prototype._DrawFocusItem = function(id, container_view) {
        var item_date = this.data[id];

        // Add to metro
        var scaled_view = new Forge.LayoutView();

        if(this.IsFocus()){
            if(!this._IsPress) {
                var bg_image = "./images/focus.png";
            }else{
                var bg_image = "./images/focus_press.png";
            }
        }else{
            var bg_image = "./images/select-mengban.png"
        }

        var focus_texture = this._TextureManager.GetImage2(bg_image,false,{width:84,height:85},"RGBA_8888");
        var focus_bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(focus_texture));
        scaled_view.AddView(focus_bg_view,{x:0,y:0,width:84,height:85});

        var scene = this._Template._MainScene;

 
        if(id == 1 && scene.IsCollection(scene._CurrRadioData)){
            var image_url = "./images/shoucang.png";
        }else{
            var image_url = item_date.focus_image;
        }

        var image_texture = this._TextureManager.GetImage2(image_url,false,{width:84,height:85},"RGBA_8888");
        var image_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(image_texture,null,null,true));
        scaled_view.AddView(image_view,{x:0,y:0,width:84,height:85});

         var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(28,undefined,"center","middle","#ffffff",new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, 30),30),item_date.title);
        scaled_view.AddView(text_viewex.GetLayoutView(), {x:0, y:90});
       
        container_view.AddView(scaled_view, {
            width:CONST_ITEM_WIDTH,
            height:CONST_ITEM_HEIGHT
        });
      

        return new Forge.FocusDrawingResult(scaled_view, 1, null);
    };

    // BlockRadioOper.prototype._DrawBlurItem = function(id, container_view) {
    //     // Add to metro
    //     var blured_view = new Forge.LayoutView();
    //     container_view.AddView(blured_view, {
    //         width:CONST_ITEM_WIDTH,
    //         height:CONST_ITEM_HEIGHT
    //     });

    //     this._DrawItemView(id, container_view)
    //     return new Forge.FocusDrawingResult(blured_view, 1, null);
    // };

    BlockRadioOper.prototype._Click = function(id) {
        var _this = this;

        console.log("Item clicked id=" + id + ", name=");
        var scene = this.GetParentPage();

        this._IsPress = true;
        this.RequestFocus(id);
        setTimeout(function(){
            _this._IsPress = false;
            _this.RequestFocus(id);
        },100);


        switch(id){
            case 0:
                scene.ControlAllClipViewStatus("kind");
                break;
            case 1:
                ;
                if(scene.IsCollection(scene._CurrRadioData)){
                    scene.RemoveItemWithCollection(scene._CurrRadioData);
                    this._HostActivity._PageUtils.Toast("已从收藏中移除", 2000);

                }else{
                    scene.AddCollection(scene._CurrRadioData);
                    this._HostActivity._PageUtils.Toast("已添加至收藏电台", 2000);
                }
                this.RequestRedraw(1);
                this.RequestFocus(1);

                break;
            case 2:
                if(scene.CurrStatus.show != null && scene.CurrStatus.show != -1 && scene.ViewIsHide(scene._ShowsClipView)){
                    if(scene._RadioShowsBlock._SellectShowId == null){
                        scene._RadioShowsBlock.SetEnteringFocus(scene.CurrStatus.show);
                        scene._RadioShowsBlock.RequestFocus(scene.CurrStatus.show);
                    }else{
                        scene._RadioShowsBlock.SetEnteringFocus(scene._RadioShowsBlock._SellectShowId);
                        scene._RadioShowsBlock.RequestFocus(scene._RadioShowsBlock._SellectShowId);
                    }

                }
                scene.ControlAllClipViewStatus("show");
                break;
        }
       

    };

    // Override OnFocus
    BlockRadioOper.prototype.OnFocus = function() {
        this.SetEnteringFocus(this.GetMetroFocusId());
        BlockRadioOper.__super__.OnFocus.call(this);
    };

    return BlockRadioOper;
}(Forge.MetroWidget));