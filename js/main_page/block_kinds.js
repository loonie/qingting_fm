WebappTemplate.BlockRadioKinds= (function(_super) {
    __extends(BlockRadioKinds, _super);

    var CONST_ITEM_WIDTH = 144;
    var CONST_ITEM_HEIGHT = 60;
    // var CONST_WIDTH_GAP = 6;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 0;
    var CONST_SCALE = 1;

    function BlockRadioKinds(activity, block_root_view,template) {
        BlockRadioKinds.__super__.constructor.call(this, "BlockRadioKinds");

        this._HostActivity = activity;
        this._Template = template;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._PageManager = this._HostActivity._PageManager;
        this._PageUtils = this._HostActivity._PageUtils;
        this._DataMain = template._DataMain;
        this._Timer  = null;

    
    };

    BlockRadioKinds.prototype.OnKeyDown = function(ev) {
        var pre_focus = this.GetMetroFocusId();
        var key_used = BlockRadioKinds.__super__.OnKeyDown.call(this, ev);

        var scene = this.GetParentPage();
        var focus_id = this.GetMetroFocusId();
        if(pre_focus == 1 && pre_focus != focus_id){
            scene._Data[1].list = scene._HistoryData;
            if(scene.CurrStatus.selectKind == 1)scene.CurrStatus.radioId = 0;
        }

        var _this = this;
        if(key_used){
            if(ev.keyCode == 38 || ev.keyCode == 40){
                if(focus_id == scene.CurrStatus.kind) return;

                if(this._Timer != null)clearTimeout(this._Timer);
                    this._Timer = setTimeout(function(){
                        _this.RedrawSelectStatus(focus_id);

                        if( (typeof scene._Data[focus_id].list == "undefined" || scene._Data[focus_id].list == null) && focus_id !=0 &&focus_id != 1){
                            var params={
                                id:scene._Data[focus_id].id,
                                page: 1,
                            }

                            scene._DataMain.GetRadioData(params,function(data){
                                scene._Data[focus_id].list = data.items;
                                scene._Data[focus_id].total = data.total;
                                scene._RadiosBlock._UpdateBlock(scene._Data[focus_id].list, scene._Data[focus_id].total);
                        
                            },function(data){

                            })

                        }else{
                            if(focus_id == 1){
                                scene._HistoryData = scene._Data[focus_id].list;
                            }

                            if(scene._RadiosBlock.data.length>0){
                                scene._RadiosBlock._UpdateBlock(scene._Data[focus_id].list); 
                            }else{
                                scene._RadiosBlock._CreateBlock(scene._Data[focus_id].list);
                            }                              
        
                        }       
                    },400)    
            }

        }else if(ev.keyCode == 39){
          
            if(scene._RadiosBlock.data!= null && scene._RadiosBlock.data.length>0){
                scene._RadiosBlock.Focus();
            }
       
        }
        return false;
    };
    BlockRadioKinds.prototype._CreateBlock = function(data){

        this._BuildMetro(data);
    }

    BlockRadioKinds.prototype._BuildMetro = function(data) {
        this.data = data
        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: 0,
            heightGap: 0
        };

        var metro_setting = {
            // margin:{"right":20,"left":20},
            direction:"vertical",
            lineMax: 1,
            visibleLength: 8.8,
            //visibleRect:new Forge.RectArea(0, 0, 1240, CONST_ITEM_HEIGHT),
            count: this.data.length,
            slideStyle:"seamless"
        };

        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(255,255,255,0)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(0, 0);
        var focus_setting = {
            useClippedFocusFrame:true,
            focusFrame:focus_frame,
            duration:250
            
        };

        var _this = this;
        var callbacks = {
            onDraw:function() { return _this._DrawItem.apply(_this, arguments); },
            onClick:function() { return _this._Click.apply(_this, arguments); },
            onDrawFocus:function() { return _this._DrawFocusItem.apply(_this, arguments) },
            onDrawBluring:null,
            // onDrawBluring:function() { return _this._DrawBlurItem.apply(_this, arguments) },
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };

    BlockRadioKinds.prototype._DrawItemView = function(id, container_view,is_focus){
        var item_date = this.data[id];
        var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(28,undefined,"center","middle",this._Template.TextColor,new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, CONST_ITEM_HEIGHT),CONST_ITEM_HEIGHT),item_date.title);
        container_view.AddView(text_viewex.GetLayoutView(), {x:0, y:0});
    }

    BlockRadioKinds.prototype._DrawItem = function(id, container_view) {


        var child_view = new Forge.LayoutView();

        if(id == this.GetParentPage().CurrStatus.kind){
            var select_bg_texture = this._TextureManager.GetImage2("./images/kind_select.png",false,{width:CONST_ITEM_WIDTH,height:CONST_ITEM_WIDTH},"RGBA_8888");
            var bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(select_bg_texture));
            child_view.AddView(bg_view,{x:0,y:0,width:CONST_ITEM_WIDTH,height:CONST_ITEM_HEIGHT});
        }
        // Add to metro
        container_view.AddView(child_view, new LayoutParams({
            width: CONST_ITEM_WIDTH,
            height: CONST_ITEM_HEIGHT
        }));
        this._DrawItemView(id, container_view)
    };

    BlockRadioKinds.prototype._DrawFocusItem = function(id, container_view) {
        // Add to metro
        var scaled_view = new Forge.LayoutView();

        var focus_bg_texture = this._TextureManager.GetImage2("./images/kinds_focus.png",false,{width:CONST_ITEM_WIDTH,height:CONST_ITEM_WIDTH},"RGBA_8888")
        var bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(focus_bg_texture));
        scaled_view.AddView(bg_view,{x:0,y:0,width:CONST_ITEM_WIDTH,height:CONST_ITEM_HEIGHT});
      
        container_view.AddView(scaled_view, {
            width:CONST_ITEM_WIDTH,
            height:CONST_ITEM_HEIGHT
        });
        this._DrawItemView(id, container_view,true);

        return new Forge.FocusDrawingResult(scaled_view, 1, null);
    };

    // BlockRadioKinds.prototype._DrawBlurItem = function(id, container_view) {
    //     // Add to metro
    //     var blured_view = new Forge.LayoutView();
    //     container_view.AddView(blured_view, {
    //         width:CONST_ITEM_WIDTH,
    //         height:CONST_ITEM_HEIGHT
    //     });

    //     this._DrawItemView(id, container_view)
    //     return new Forge.FocusDrawingResult(blured_view, 1, null);
    // };

    BlockRadioKinds.prototype._Click = function(id) {



    };

    // Override OnFocus
    //BlockRadioKinds.prototype.OnFocus = function() {
    //    this.SetEnteringFocus(this.GetMetroFocusId());
    //    BlockRadioKinds.__super__.OnFocus.call(this);
    //};
    BlockRadioKinds.prototype.RedrawSelectStatus = function(id){
        var  temp  = this.GetParentPage().CurrStatus.kind;
        this.GetParentPage().CurrStatus.kind = id;
        this.RequestRedraw(id);
        this.RequestRedraw(temp);
        this.RequestFocus(id);


    };

    return BlockRadioKinds;
}(Forge.MetroWidget));