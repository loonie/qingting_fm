WebappTemplate.BlockRadios= (function(_super) {
    __extends(BlockRadios, _super);

    var CONST_ITEM_WIDTH = 304;
    var CONST_ITEM_HEIGHT = 54  ;
    // var CONST_WIDTH_GAP = 6;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 6;
    var CONST_SCALE = 1;

    function BlockRadios(activity, block_root_view,template) {
        BlockRadios.__super__.constructor.call(this, "BlockRadios");

        this._HostActivity = activity;
        this._Template = template;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._PageManager = this._HostActivity._PageManager;
        this._PageUtils = this._HostActivity._PageUtils;
        this._DataMain = template._DataMain;

    
    }

    BlockRadios.prototype.OnKeyDown = function(ev) {
        var key_used = BlockRadios.__super__.OnKeyDown.call(this, ev);
        var id = this.GetMetroFocusId();
        if(!key_used){
            var scene = this.GetParentPage();
            switch(ev.keyCode){
                case 37:
                    //left
                    scene._RadioKindsBlock.Focus();
                    break;
                case 39:
                    //right
                     scene._RadioOperBlock.Focus();
                    break;
            }
        }else{
            if(this.total && ev.keyCode == 40 && this.data.length < this.total && id == this.data.length - 6 ){
                var scene = this.GetParentPage();
                var params = {
                    page: this.data.length/50 + 1,
                    id: scene._Data[scene.CurrStatus.kind].id,
                }   
                var _this = this;
                scene._DataMain.GetRadioData(params, function(result){
                    var data = result.items;
                    _this._AppendBlock(data);
                    data = data.concat(scene._Data[scene.CurrStatus.kind].list, data);
                    scene._Data[scene.CurrStatus.kind].list = data;
                    
                })
                
            }
        }
        return false;
    };
    BlockRadios.prototype._CreateBlock = function(data,total){
        this.total = total;
        this.data = data;
        if(data.length == 0){

            this.DrawEmptyView();
            return;
        }

        this._ClearBlock();
        this._BuildMetro(data);
    }
    BlockRadios.prototype._ClearBlock = function(){
        if(this._MainView && this._MainView.ChildViews.length>0)
            this._MainView.ClearViews();
    }

    BlockRadios.prototype._BuildMetro = function(data) {
        
        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };


        this._TemplateTmp = {
            "list": [
            ],
            "orientation": {
                "type": "vertical",
                "lineMaxUnits":304
            }
        };
        for (var i = 0; i < this.data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT+6
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {

            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 304, 540),
            slideStyle: "seamless",
            
        };


        var frame_color_texture = this._TextureManager.GetColorTexture(this._Template.TextColor);
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(0, 0);
        var focus_setting = {
            useClippedFocusFrame:true,
            focusFrame:focus_frame,
            duration:250
            
        };

        var _this = this;
        var callbacks = {
            onDraw:function() { return _this._DrawItem.apply(_this, arguments); },
            onClick:function() { return _this._Click.apply(_this, arguments); },
            onDrawFocus:function() { return _this._DrawFocusItem.apply(_this, arguments) },
            onDrawBluring:null,
            // onDrawBluring:function() { return _this._DrawBlurItem.apply(_this, arguments) },
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };


    BlockRadios.prototype._DrawItem = function(id, container_view) {
        var item_date = this.data[id];

        var child_view = new Forge.LayoutView();
        // Add to metro
        var color  = null,name = null;
        var curr_status = this.GetParentPage().CurrStatus;
        if(item_date.content_id == curr_status.radio  && curr_status.selectKind == curr_status.kind ){
            color = "#00e3db";
            name = item_date.title + " (正在收听)";
        }else{
            color = "#f1f1f1";
            name = item_date.title
        }

        var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(24,undefined,"left","middle",color,new Forge.RectArea(0, 0, 256, CONST_ITEM_HEIGHT),CONST_ITEM_HEIGHT),name);
        child_view.AddView(text_viewex.GetLayoutView(), {x:20, y:0});

        container_view.AddView(child_view, new LayoutParams({
            width: CONST_ITEM_WIDTH,
            height: CONST_ITEM_HEIGHT
        }));
        
    };

    BlockRadios.prototype._DrawFocusItem = function(id, container_view) {

        var item_data = this.data[id];
        // Add to metro
        var scaled_view = new Forge.LayoutView();
      
        var focus_bg_texture = this._TextureManager.GetImage2("./images/radio_focus.png",false,{width:CONST_ITEM_WIDTH,height:CONST_ITEM_HEIGHT},"RGBA_8888");
        var bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(focus_bg_texture));
        scaled_view.AddView(bg_view,{x:0,y:0,width:CONST_ITEM_WIDTH,height:CONST_ITEM_HEIGHT});

        var name = item_data.title;
        var curr_status = this.GetParentPage().CurrStatus;
        if(item_data.content_id == curr_status.radio &&  curr_status.selectKind == curr_status.kind){
            name  += " (正在收听)";
        }

         var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(24,undefined,"left","middle","#ffffff",new Forge.RectArea(0, 0, 256, CONST_ITEM_HEIGHT),CONST_ITEM_HEIGHT,true),name);
        scaled_view.AddView(text_viewex.GetLayoutView(), {x:20, y:0});
        
        container_view.AddView(scaled_view, {
            width:CONST_ITEM_WIDTH,
            height:CONST_ITEM_HEIGHT
        });

        return new Forge.FocusDrawingResult(scaled_view, 1, null);
    };

    // BlockRadios.prototype._DrawBlurItem = function(id, container_view) {
    //     // Add to metro
    //     var blured_view = new Forge.LayoutView();
    //     container_view.AddView(blured_view, {
    //         width:CONST_ITEM_WIDTH,
    //         height:CONST_ITEM_HEIGHT
    //     });

    //     this._DrawItemView(id, container_view)
    //     return new Forge.FocusDrawingResult(blured_view, 1, null);
    // };

    BlockRadios.prototype._Click = function(id) {
        var _this = this;
        var item_data = this.data[id];


        console.log("Item clicked id=" + id + ", name="+item_data.name);

        var scene = this.GetParentPage();
        if(id == scene.CurrStatus.radioId && scene.CurrStatus.selectKind == scene.CurrStatus.kind){
            return;
        }
        scene.CurrStatus.selectKind = scene.CurrStatus.kind;
        var pre_id = scene.CurrStatus.radioId;
        scene.CurrStatus.radioId = id;
        scene.CurrStatus.radio = item_data.content_id;
        scene._CurrRadioData = item_data;


        this.RequestRedraw(pre_id);
        this.RequestRedraw(id);

        this.RequestFocus(id);

        //var src = "http://http.qingting.fm/"+item_data.urls[0]+".mp3";
        var src = audioControl.dealRadioLiveSrc(item_data.content_id);

        audioControl.play(src);

        if(this.GetParentPage().CurrStatus.kind != 1){
            //添加浏览记录
            scene.AddHistory(item_data);
        }


        //重绘电台信息界面  
        scene.DrawRadioInfoView(item_data);

        //重绘收藏状态
        scene._RadioOperBlock.RequestRedraw(1);

        //绘制电台节目
        scene.GetRadioShowData();
       

    };

    // Override OnFocus
    BlockRadios.prototype.OnFocus = function() {
        this.SetEnteringFocus(this.GetMetroFocusId());
        BlockRadios.__super__.OnFocus.call(this);
    };
    BlockRadios.prototype._AppendBlock = function(data){

        var templateTmp = {
            "list": [
            ],
            "orientation": {
                "type": "vertical",
                "lineMaxUnits": 304
            }
        };
        
        for (var i = 0; i < data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            templateTmp.list.push(obj);
            this.data.push(data[i]);
            }
            var block = this.GetMetroLayout();
            block.AppendTemplate(templateTmp);
    };
    BlockRadios.prototype._UpdateBlock = function(data,total){
        this.total = total;
        this.data = data;
        if(data.length == 0){
            this.DrawEmptyView();
            return;
        }

        this._TemplateTmp = {
            "list": [
            ],
            "orientation": {
                "type": "vertical",
                "lineMaxUnits": 304
            }
        };
        
        for (var i = 0; i < data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }
        var visily_rect = new Forge.RectArea(0, 0, 304, 540);
        this.UpdateTemplate(this._TemplateTmp,visily_rect,false);
    };

    BlockRadios.prototype.DrawEmptyView = function(){
        this._ClearBlock();

        var error_logo_texture = this._TextureManager.GetBase64Image(this._DataMain.Main_data.ERROR_LOGO);
        var error_logo_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(error_logo_texture));
        this._MainView.AddView(error_logo_view,{x:107,y:224,width:89,height:39   });

        var text = "暂时没有电台";
        if(this.GetParentPage().CurrStatus.kind == 0){
            text = "暂时没有收藏电台";
        }else if(this.GetParentPage().CurrStatus.kind ==1){
            text = "暂时没有收听电台";
        }

        var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(24,undefined,"center","middle","#989a9e",new Forge.RectArea(0, 0, 304, 50),50,true),text);
        this._MainView.AddView(text_viewex.GetLayoutView(), {x:0, y:270});
    };




    return BlockRadios;
}(Forge.MetroWidget));