WebappTemplate.BlockShows= (function(_super) {
    __extends(BlockShows, _super);

    var CONST_ITEM_WIDTH = 354;
    var CONST_ITEM_HEIGHT = 54;
    // var CONST_WIDTH_GAP = 6;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 0;
    var CONST_SCALE = 1;

    function BlockShows(activity, block_root_view,template) {
        BlockShows.__super__.constructor.call(this, "BlockShows");

        this._HostActivity = activity;
        this._Template = template;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._PageManager = this._HostActivity._PageManager;
        this._PageUtils = this._HostActivity._PageUtils;
        this._DataMain = template._DataMain;
        this._SellectShowId = null;
        _shows = this;
        audioControl.beforeSHowEndPlayLive = this.BeforeSHowEndPlayLive;
    
    }

    BlockShows.prototype.OnKeyDown = function(ev) {
        var key_used = BlockShows.__super__.OnKeyDown.call(this, ev);

            if(!key_used && ev.keyCode == 37){
                var scene = this.GetParentPage();
                scene._RadioOperBlock.Focus();
       
            }
        return false;
    };
    BlockShows.prototype._CreateBlock = function(data){

        // data = this.GetCurrentDayShowDate(data);

        if(data.length==0  ||(data.length == 1 && data[0].name == "暂无节目单")){
            this.DrawEmptyView();
        }else{
            this._ClearBlock();
            this._BuildMetro(data);
        }
    };
    BlockShows.prototype._ClearBlock = function(){
        if(this._MainView && this._MainView.ChildViews.length>0){
            this._MainView.ClearViews();
        }
        this.data = [];
        this.GetParentPage().CurrStatus.show = null;
        this._SellectShowId = null;
    };

    BlockShows.prototype._BuildMetro = function(data) {
        this.data = data;

        var scene = this.GetParentPage();
        scene.CurrStatus.show = this.GetCurrenShowId(this.data);
        this._SellectShowId = scene.CurrStatus.show;

        if( this._SellectShowId != -1){
            var title = this.data[scene.CurrStatus.show].name
        }else{
            var title =" ";
        }
           scene.DrawRadioShowTitleView(title);
        
    
        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: 0,
            heightGap: 0
        };

              this._TemplateTmp = {
            "list": [
            ],
            "orientation": {
                "type": "vertical",
                "lineMaxUnits":354
            }
        };
        for (var i = 0; i < this.data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {

            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 354, 540),
            slideStyle: "seamless"
            
        };

        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(255,255,255,0)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(0, 0);
        var focus_setting = {
            useClippedFocusFrame:true,
            focusFrame:focus_frame,
            duration:250
            
        };

        var _this = this;
        var callbacks = {
            onDraw:function() { return _this._DrawItem.apply(_this, arguments); },
            onClick:function() { return _this._Click.apply(_this, arguments); },
            onDrawFocus:function() { return _this._DrawFocusItem.apply(_this, arguments) },
            onDrawBluring:null,
            // onDrawBluring:function() { return _this._DrawBlurItem.apply(_this, arguments) },
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };

    // BlockShows.prototype._DrawItemView = function(id, container_view,is_focus){
    //     var item_date = this.data[id];
    //     var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(26,undefined,"center","middle",this._Template.TextColor,new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, CONST_ITEM_HEIGHT),CONST_ITEM_HEIGHT),item_date.title);
    //     container_view.AddView(text_viewex.GetLayoutView(), {x:0, y:0});
    // }

    BlockShows.prototype._DrawItem = function(id, container_view) {
        var item_data = this.data[id];

        var child_view = new Forge.LayoutView();
        var color = "#f1f1f1";

        var time_str = item_data.start_time.substring(11,16);

        var title = time_str + "  "+item_data.name;
        var color = "#f1f1f1";
        // if(id == this.GetParentPage().CurrStatus.show){
        //   title   +=  "  (正在直播中)";
        // }else if(id < this.GetParentPage().CurrStatus.show){
        //     color = "#f1f1f1"
        // }else{
        //     color = "#919293";
        // }
        // if(id == this._SellectShowId){
        //     color = "#00e3db";
        // }

        var compare_value = this.CompareTime(item_data.start_time,item_data.end_time);
        if(compare_value == 0){
            title   +=  "  (正在直播中)";
        }else if(compare_value == -1){
            color = "#f1f1f1"
        }else{
            color = "#919293";
        }

        if(id == this._SellectShowId){
            color = "#00e3db";
        }



        var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(24,undefined,"left","middle",color,new Forge.RectArea(0, 0, 310, CONST_ITEM_HEIGHT),CONST_ITEM_HEIGHT),title);

        child_view.AddView(text_viewex.GetLayoutView(),{x:20,y:0});

        // Add to metro
        container_view.AddView(child_view, new LayoutParams({
            width: CONST_ITEM_WIDTH,
            height: CONST_ITEM_HEIGHT
        }));
    };

    BlockShows.prototype._DrawFocusItem = function(id, container_view) {
         var item_data = this.data[id];

        // Add to metro
        var scaled_view = new Forge.LayoutView();
        var time_str = item_data.start_time.substring(11,16);

        var title = time_str + "  "+item_data.name;
        var color = "#ffffff";

        if(this.IsFocus()){
            var bg_texture = this._TextureManager.GetImage2("./images/show_focus.png",false,{width:CONST_ITEM_WIDTH,height:CONST_ITEM_HEIGHT},"RGBA_8888");
            var bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(bg_texture));
            scaled_view.AddView(bg_view,{x:0,y:0,width:CONST_ITEM_WIDTH,height:CONST_ITEM_HEIGHT});
        }else{
            color = color = "#00e3db";
        }

        if(id == this.GetParentPage().CurrStatus.show){
          title  +=  "  (正在直播中)" 
        }
        var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(24,undefined,"left","middle",color,new Forge.RectArea(0, 0, 310, CONST_ITEM_HEIGHT),CONST_ITEM_HEIGHT,true),title);

        scaled_view.AddView(text_viewex.GetLayoutView(),{x:20,y:0});


        container_view.AddView(scaled_view, {
            width:CONST_ITEM_WIDTH,
            height:CONST_ITEM_HEIGHT
        });

        return new Forge.FocusDrawingResult(scaled_view, 1, null);
    };

    // BlockShows.prototype._DrawBlurItem = function(id, container_view) {
    //     // Add to metro
    //     var blured_view = new Forge.LayoutView();
    //     container_view.AddView(blured_view, {
    //         width:CONST_ITEM_WIDTH,
    //         height:CONST_ITEM_HEIGHT
    //     });

    //     this._DrawItemView(id, container_view)
    //     return new Forge.FocusDrawingResult(blured_view, 1, null);
    // };

    BlockShows.prototype._Click = function(id) {
        var _this = this;
        var scene = this.GetParentPage();
        var item = this.data[id];
        var compare_value = this.CompareTime(item.start_time,item.end_time);

        // if(id == scene.CurrStatus.show ){
        if(compare_value == 0){
            if(audioControl.isPlayBeforeShow && id !=this._SellectShowId){
                var src = audioControl.dealRadioLiveSrc(scene.CurrStatus.radio)
                audioControl.play(src);
                this.RedrawSelectStatus(id);
                var  title = this.data[id].name;
                scene.DrawRadioShowTitleView(title);
            }
            //if(audioControl.audio.src.indexOf("m4a")>0){
            //    var src = "http://http.qingting.fm/"+scene.CurrStatus.radio+".mp3";
            //    audioControl.play(src);
            //    this.RedrawSelectStatus(id);
            //    var  title = this.data[id].title;
            //    scene.DrawRadioShowTitleView(title);
            //}

        // }else if(id < scene.CurrStatus.show){
        }else if(compare_value == -1){
            var item = this.data[id];
            var src_params = this.DealBeforeShowsSrcParams(item);
            //var src = "http://cacheshare.qingting.fm/cacheshare/"+src_params+".m4a";
            var src = audioControl.delRadioBeforeShowSrc(src_params);
            audioControl.play(src);

            this.RedrawSelectStatus(id);
            var  title = this.data[id].name;
            scene.DrawRadioShowTitleView(title);
        // }else {
        }else if(compare_value == 1){
            //提示 节目还没播出
            this._PageUtils.Toast("该节目尚未播出",2000);
        }

    };

    //// Override OnFocus
    //BlockShows.prototype.OnFocus = function() {
    //    this.SetEnteringFocus(this.GetMetroFocusId());
    //    BlockShows.__super__.OnFocus.call(this);
    //};

    BlockShows.prototype.GetCurrenShowId = function(data){
        var curr_time = (new Date).GetCurrentTime();
        for(var i=0,len=data.length;i<len;i++){
            var item = data[i];
            var result = this.CompareTime(item.start_time,item.end_time);
            if(result == 0){
                return i;
            }
        }
        return -1;
    };

    BlockShows.prototype.CompareTime = function(starTime,endTime){
         var starTimeArr =starTime.substring(11).split(":");
         var endTimeArr = endTime.substring(11).split(":");
         var currTimeArr = (new Date()).GetCurrentTime().split(":");

         var s_time = parseInt(starTimeArr[0])*60*60+parseInt(starTimeArr[1])*60+parseInt(starTimeArr[2]);
         var e_time = parseInt(endTimeArr[0])*60*60+parseInt(endTimeArr[1])*60+parseInt(endTimeArr[2]);
         var c_time = parseInt(currTimeArr[0])*60*60+parseInt(currTimeArr[1])*60+parseInt(currTimeArr[2]);   

         if(s_time<= c_time && c_time<= e_time){
            return 0;
         } 
         if(c_time<s_time ){
            return 1;
         }
         if(c_time >e_time){
            return -1;
         }

    };

    BlockShows.prototype._UpdateBlock = function(data){
        // this.data =  this.GetCurrentDayShowDate(data);
        this.data = data;
        if( this.data.length==0  ||(this.data.length == 1 &&(this.data[0].title == "暂无节目单"||this.data[0].title ==" "))){
            this.DrawEmptyView();
            return;
        }


        var scene = this.GetParentPage();
        scene.CurrStatus.show = this.GetCurrenShowId(this.data);

        this._SellectShowId = scene.CurrStatus.show;
        if(scene.CurrStatus.show  != -1){
            var title = this.data[scene.CurrStatus.show].title
        }else{
            var title = " "
        }
        scene.DrawRadioShowTitleView(title);
        this._TemplateTmp = {
            "list": [
            ],
            "orientation": {
                "type": "vertical",
                "lineMaxUnits": 354
            }
        };
        
        for (var i = 0; i < this.data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }
        var visily_rect = new Forge.RectArea(0, 0, 354, 540);
        this.UpdateTemplate(this._TemplateTmp,visily_rect,false);
    };


    BlockShows.prototype.GetCurrentDayShowDate = function(data){
        var day =(new Date()).getUTCDay()+1
            for(var item in data){
                if(item == day){
                   return data[item];
                }
            }
    };
    BlockShows.prototype.DealBeforeShowsSrcParams = function(item){
        var src ="",src_params="";
        // if(audioControl.audio.canPlayType("audio/mp4; codecs=\"mp4a.40.29\"") == "probably"){
        //     src_params = this.DealBeforeShowsM3U8SrcParams(item);
        //     //src =" http://cacheshare.qingting.fm/cacheshare/"+src_params+".m4a";
        // }else{
        //     src_params = this.DealBeforeShowsMP3SrcParams(item);
        //     //src = "http://hls.qd.qingting.fm/cache/"+src_params;
        // }
        src_params = this.DealBeforeShowsAccSrcParam(item);

        // if(typeof  jContentShellJBridge != "undefined" && jContentShellJBridge.getDeviceInfo ){
        //     var device_info =JSON.parse(jContentShellJBridge.getDeviceInfo());
        //     if(device_info.MODEL.indexOf("MagicBox_M16C") >=0 ){
        //        src_params = this.DealBeforeShowsMP3SrcParams(item);
        //     }
        //     if(device_info.MODEL.indexOf("Allwinner") >= 0) {
		// 		src_params = this.DealBeforeShowsM3U8SrcParams(item);
		// 	}
        // }   

        return src_params;
    };

    BlockShows.prototype.DealBeforeShowsMP3SrcParams = function(item){
        var date = new Date();
        function timeTwoSize(str){
            if(typeof str != "string" ){
                str = str.toString();
            }
            if(str.length<2){
                str = "0"+str;
            }
            return str;
        }

        //386-20160802_020000-20160802_020500
        var radioid = (this.GetParentPage().CurrStatus.radio).toString();
        var year = date.getFullYear();
        var mouth =timeTwoSize(date.getMonth()+1);
        var day =  timeTwoSize(date.getDate());
        var  time = year+mouth+day;


        var start_time = item.start_time.replace(/\:/g,"");
        var end_time = item.end_time.replace(/\:/g,"");

        var src_params = radioid+"-"+time+"_"+start_time+"-"+time+"_"+end_time;
        return src_params;
    };

    BlockShows.prototype.DealBeforeShowsM3U8SrcParams = function(item){

        function timeTwoSize(str){
            if(typeof str != "string" ){
                str = str.toString();
            }
            if(str.length<2){
                str = "0"+str;
            }
            return str;
        }
        var radioid = (this.GetParentPage().CurrStatus.radio).toString();
        var date = new Date();
        var year = (date.getFullYear()).toString().substr(2,2);
        var mouth =timeTwoSize(date.getMonth()+1);
        var day =  timeTwoSize(date.getDate());
        var time = year+"M"+mouth+"D"+day;

        var start_time_arr = item.start_time.split(":");
        var statTime = "h"+start_time_arr[0]+"m"+start_time_arr[1]+"s"+start_time_arr[2];
        var end_time_arr = item.end_time.split(":");
        var endTime = "h"+end_time_arr[0]+"m"+end_time_arr[1]+"s"+end_time_arr[2];

        var src_params = radioid+".m3u8?start="+time+statTime+"&end="+time+endTime+"&format=mpegts";

        return src_params;

    };
    BlockShows.prototype.DealBeforeShowsAccSrcParam = function(item){
         //20170320/388/388_20170320_000000_055500_24_0.aac
         var start_time_arr = item.start_time.substr(11,8).split(":");
         var statTime = start_time_arr[0].toString() + start_time_arr[1] +start_time_arr[2];

        var end_time_arr = item.end_time.substr(11,8).split(":");
        var endTime = end_time_arr[0].toString() + end_time_arr[1] + end_time_arr[2];

         var date = (new Date()).GetCurrentDate();
         var radioid = (this.GetParentPage().CurrStatus.radio).toString();


         var src_params = date+"/"+radioid+"/"+radioid+"_"+date+"_"+statTime+"_"+endTime+"_24_0.aac";
         return src_params;
    }

    BlockShows.prototype.RedrawSelectStatus = function(id){
        var temp = this._SellectShowId;
        this._SellectShowId = id;
        this.RequestRedraw(id);
        this.RequestRedraw(temp);
    };

    BlockShows.prototype.DrawEmptyView = function(){
        this._ClearBlock();

        var error_logo_texture = this._TextureManager.GetBase64Image(this._DataMain.Main_data.ERROR_LOGO);
        var error_logo_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(error_logo_texture));
        this._MainView.AddView(error_logo_view,{x:107,y:224,width:89,height:39  });

        var text = "暂无节目单";

        var text_viewex = new Forge.TextViewEx(this._TextureManager, this._PageUtils.GetTextSetting(24,undefined,"center","middle","#989a9e",new Forge.RectArea(0, 0, 304, 50),50,true),text);
        this._MainView.AddView(text_viewex.GetLayoutView(), {x:0, y:270});

        this.GetParentPage().DrawRadioShowTitleView("  ");
    };
    BlockShows.prototype.BeforeSHowEndPlayLive = function(){
        _shows.RedrawSelectStatus(_shows.GetParentPage().CurrStatus.show);
    }



    return BlockShows;
}(Forge.MetroWidget));