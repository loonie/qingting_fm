WebappTemplate.MainScene = (function(_super) {
    __extends(MainScene, _super);

    function MainScene(template, block_root_view) {
        MainScene.__super__.constructor.call(this, "MainScene");

        this._HostActivity = template._HostActivity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._PageUtils = this._HostActivity._PageUtils;
        this._MainView = block_root_view;
        this._Template = template;
        this._DataMain = template._DataMain;
        this._Data = null;
        this._HistoryData = [];
        this._CurrRadioData = null;

        this._PageUtils.SetToastSetting({
            "TextSize":28,
            "TextColor":"#ffffff",
            "PaddingLeft":90,
            "PaddingTop":26,
            "BackgroundColor":"rgba(0,0,0,0.8)",
            "MarginTop":580,
            "RoundCorner": 40
        })

        audioControl.init(this.AudioToast);
        this._BuildMetro();
        _this = this;
    }

    MainScene.prototype._BuildMetro = function() {
        var _this = this;
        var categorieslist_view = new Forge.LayoutView();

        var icon_image_texture = this._TextureManager.GetImage2("./images/logo.png",true,{width:98,height:38},"RGBA_8888");
        var icon_image_view = new LayoutView();
        icon_image_view.Init(new TextureSetting(icon_image_texture, null, null, true));
        this._MainView.AddView(icon_image_view, {x:1140, y:647, width:98, height:38});


        this._PageUtils.ShowLoading();


        var _this = this;
        // var success = function(data){
        this._Data = _this._DataMain.GetUserSaveData(window.RadioKinds);
        var params = {
            id: this._Data[2].id,
            page: 1,
        }
        this._DataMain.GetRadioData(params,function(data){
                _this._Data[2].list = data.items;
                _this._Data[2].total = data.total;
                _this.DrawMainView();
                _this._PageUtils.HideLoading();

        },function(data){

        });
        // };
        // var error = function(){
        //     var data = localStorage.getItem("QingTingFm_Data");
        //     if(typeof data!= "undefined" && typeof data == "string" && data != "undefined"){
        //         data = JSON.parse(data);
        //         if(data.length>0) {
        //             _this._Data = data;
        //             _this._Data = _this._DataMain.GetUserSaveData(data);
        //             _this.DrawMainView();
        //             _this._PageUtils.HideLoading();
        //             return;
        //         }
        //     }
        //     _this._DataMain.GetRadioKindsData(success,error);
        // };

        // this._DataMain.GetRadioKindsData(success,error);

    };

    MainScene.prototype.DrawMainView = function(data){

        this.CurrStatus={
            selectKind:2,
            kind:2,
            radioId:0,
            radio:this._Data[2].list[0].content_id,
            show:null
        }
        this._CurrRadioData = this._Data[2].list[0];

        this.DrawRadioKindsView();
        this.DrawRadioShowsView(); 
        this.DrawRadioInfoView(this._CurrRadioData);
        this.GetRadioShowData();

        var src = audioControl.dealRadioLiveSrc(this._Data[2].list[0].content_id);
        audioControl.play(src);
        setTimeout(function(){
            //  if(document.getElementById("loading_start")){
            // document.getElementById("loading_start").style.backgroundImage="none";
            // document.getElementById("loading_start").style.display = "none";
            if(typeof jContentShellJBridge != 'undefined' && typeof  jContentShellJBridge.indicateHomePageLoadDone == 'function'){
                jContentShellJBridge.indicateHomePageLoadDone()
            }
        },500)
    };

    MainScene.prototype.DrawRadioInfoView = function(info){

        if(typeof this._RadioView == "undefined"){
            var radio_bg_texture = this._TextureManager.GetImage2("./images/radio_bg.png",false,{width:414,height:621},"RGBA_8888");
            this._RadioView = new Forge.LayoutView(new Forge.ExternalTextureSetting(radio_bg_texture));
            this._MainView.AddView(this._RadioView,{x:432,y:50,width:414,height:621});
        }

        //default image
        if(typeof this._DefaultView == "undefined"){
            this._DefaultView = new Forge.LayoutView(new Forge.ExternalTextureSetting(this._Template._DefaultImageTexture));
            this._RadioView.AddView(this._DefaultView,{x:117,y:84,width:180,height:180});
        }

        //image
        if(this.ViewIsExist(this._RadioImageView)){
            this._RadioView.RemoveView(this._RadioImageView);
        }
        if(typeof info.cover != "undefined") {
            var radioTexture = this._TextureManager.GetImage2(info.cover, false, {width: 180, height: 180},"RGBA_8888")
            var corner_mask = new Forge.ViewRoundCornerMask(5);
            this._RadioImageView = new Forge.LayoutView(new Forge.ExternalTextureSetting(radioTexture, corner_mask));
            this._RadioView.AddView(this._RadioImageView, {x: 117, y: 84, width: 180, height: 180});
        }

        //title
        if(this.ViewIsExist(this._RadioTitleView)){
            this._RadioView.RemoveView(this._RadioTitleView);
        }
        var text_style =this._PageUtils.GetTextSetting(32,undefined,"center","middle",this._Template.TextColor,new Forge.RectArea(0, 0, 340, 34),34);
        var radioTitleEx = new Forge.TextViewEx(this._TextureManager, text_style,info.title);
        this._RadioTitleView = radioTitleEx.GetLayoutView();
        this._RadioView.AddView(this._RadioTitleView,{x:40,y:323,width:340,height:34});


        //oper block
        if(typeof this._RadioOperBlock == "undefined"){
            var radio_oper_view  = new Forge.LayoutView();
            this._RadioOperBlock= new WebappTemplate.BlockRadioOper(this._HostActivity, radio_oper_view,this._Template);
            this._RadioOperBlock.SetParentPage(this);
            this._RadioView.AddView(radio_oper_view, {x:43, y:444});
            this._RadioOperBlock._CreateBlock();
            this._RadioOperBlock.Focus();

        }      

    };

    MainScene.prototype.DrawRadioShowTitleView = function(title){
        if(this.ViewIsExist(this._RadioShowTitleView)){
            this._RadioView.RemoveView(this._RadioShowTitleView);
        }
        var text_style =this._PageUtils.GetTextSetting(24,undefined,"center","middle",this._Template.TextColor,new Forge.RectArea(0, 0, 340, 26),26);
        var radioTitleEx = new Forge.TextViewEx(this._TextureManager, text_style,title);
        this._RadioShowTitleView = radioTitleEx.GetLayoutView();
        this._RadioView.AddView(this._RadioShowTitleView,{x:40,y:287,width:340,height:26});
    };

    //电台列表区域
    MainScene.prototype.DrawRadioKindsView = function(){
        if(typeof this._KindsClipView != "object" ){

            this._KindsClipView  = new Forge.ClipView();
            this._KindsClipView.Init();
            this._KindsClipView.SetId("Kind_View"); 
            this._KindsClipView.SetClipRect(0, 0, 524, 540,true);
            this._MainView.AddView(this._KindsClipView,{x:198,y:90,width:524,height:540})

            // var color_texture = this._TextureManager.GetColorTexture("rgba(255,0,0,1)");
            var kinds_bg_texture = this._TextureManager.GetImage2("./images/radio_kings_bg.png",false,{width:524,height:540},"RGBA_8888")
            this._KindsView = new Forge.LayoutView(new Forge.ExternalTextureSetting(kinds_bg_texture));

            if(typeof this._RadioKindsBlock == "undefined"){
                var radio_kinds_block_view  = new Forge.LayoutView();
                this._RadioKindsBlock= new WebappTemplate.BlockRadioKinds(this._HostActivity, radio_kinds_block_view,this._Template);
                this._RadioKindsBlock.SetParentPage(this);
                this._KindsView.AddView(radio_kinds_block_view, {x:20, y:12});
                this._RadioKindsBlock._CreateBlock(this._Data);
                this._RadioKindsBlock.SetEnteringFocus(2)

            }   
            this.DrawRadiosBlock(this._Data[2].list, this._Data[2].total);

            this._KindsClipView.AddView(this._KindsView,{x:524,y:0,width:524,height:540});
        }
        this.RadioKindsViewIsHide(true);
    };

    MainScene.prototype.RadioKindsViewIsHide = function(isHide){
        if(typeof isHide != "undefined"){
            if(typeof this._KindsClipView == "object"){
                this._KindsClipView.SetVisibility(isHide?"HIDDEN":"VISIBLE");
            }
        }else{
            this._KindsClipView.SetVisibility(this._KindsClipView.GetVisibility() == "VISIBLE" ?"HIDDEN":"VISIBLE");
        }
    };

    MainScene.prototype.DrawRadiosBlock = function(data, total){
        if(typeof this._RadiosBlock == "undefined" ){
            var radios_block_view  = new Forge.LayoutView();
            this._RadiosBlock= new WebappTemplate.BlockRadios(this._HostActivity, radios_block_view,this._Template);
            this._RadiosBlock.SetParentPage(this);
            this._KindsView.AddView(radios_block_view, {x:200, y:0});
            this._RadiosBlock._CreateBlock(data, total); 
        }
    };
    
    //电台节目列表区域
    MainScene.prototype.DrawRadioShowsView = function(){
        if(typeof this._ShowsClipView != "object"){
            this._ShowsClipView  = new Forge.ClipView();
            this._ShowsClipView.Init();
            this._ShowsClipView.SetId("Show_View"); 
            this._ShowsClipView.SetClipRect(0, 0, 414, 540,true);
            this._MainView.AddView(this._ShowsClipView,{x:613,y:90,width:414,height:540})

            // var color_texture = this._TextureManager.GetColorTexture("rgba(0,255,0,1)");
            var shows_bg_texture = this._TextureManager.GetImage2("./images/show_bg.png",false,{width:414,height:540},"RGBA_8888");
            this._ShowsView = new Forge.LayoutView(new Forge.ExternalTextureSetting(shows_bg_texture));

            this._ShowsClipView.AddView(this._ShowsView,{x:-414,y:0,width:414,height:540});
        }
        this.RadioShowsViewIsHide(true);
    };

    //获取电台节目列表
    MainScene.prototype.GetRadioShowData = function () {
        var _this = this;
        var item_data = this._Data[this.CurrStatus.kind].list[this.CurrStatus.radioId]
        this.CurrStatus.radio = item_data.content_id;
        var showList = item_data.showList;
        var id = this._Data[this.CurrStatus.kind].list[this.CurrStatus.radioId].content_id;
        if (typeof showList == "undefined" || showList == null) {
            this._DataMain.GetRadioShowData(id, function (data) {
                if (_this.CurrStatus.kind != 1){
                    _this._Data[_this.CurrStatus.kind].list[_this.CurrStatus.radioId].showList = data;
                }else{
                    item_data.showList = data;
                    _this.AddHistoryItemData(item_data);
                }
                showList = data;
                _this.DrawRadioShowsBlock(showList);

            }, function (data) {
                debugger
            })
        } else {
            this.DrawRadioShowsBlock(showList);
            this.AddHistoryItemData(item_data);
        }
    };

    MainScene.prototype.DrawRadioShowsBlock = function(data){

        if(typeof this._RadioShowsBlock == "undefined"){
            var radio_shows_block_view  = new Forge.LayoutView();
            this._RadioShowsBlock= new WebappTemplate.BlockShows(this._HostActivity, radio_shows_block_view,this._Template);
            this._RadioShowsBlock.SetParentPage(_this);
            this._ShowsView.AddView(radio_shows_block_view, {x:30, y:0});
            this._RadioShowsBlock._CreateBlock(data);
         
        }else{
            if(this._RadioShowsBlock.data.length>0){
                this._RadioShowsBlock._UpdateBlock(data);
            }else{
                this._RadioShowsBlock._CreateBlock(data);
            }

        }

    };

    MainScene.prototype.RadioShowsViewIsHide = function(isHide){
        if(typeof isHide != "undefined"){
            if(typeof this._ShowsClipView == "object"){
                this._ShowsClipView.SetVisibility(isHide?"HIDDEN":"VISIBLE");
            }
        }else{
            this._ShowsClipView.SetVisibility(this._ShowsClipView.GetVisibility() == "VISIBLE" ?"HIDDEN":"VISIBLE");
        }
    };

    MainScene.prototype.ViewIsHide = function(view){
        var show_str = view.GetVisibility();
        if(show_str == "HIDDEN"){   
            return true;
        }else{
            return false;
        }
    };

    MainScene.prototype.GetClipViewVisibility = function(view){
        return view.GetVisibility();
    };

    MainScene.prototype.OnDestroy = function(){

        function removeShowList(list){
            for(var n=0;n<list.length;n++){
                list[n].showList=null;
            }
            return list;
        }

        var collection = removeShowList((this._Data.shift()).list);
        this._DataMain.SaveCollectionData(collection);
        var history = removeShowList((this._Data.shift()).list);
        this._DataMain.SaveHistoryData(history);
        for(var i=0;i<this._Data.length;i++){
            for(var j=0;this._Data[i].list && j<this._Data[i].list.length;j++){
                this._Data[i].list[j].showList=null;
            }

        }

        localStorage.setItem("QingTingFm_Data",JSON.stringify(this._Data))

    };

    //控制界面的移动
    MainScene.prototype.ControlAllClipViewStatus = function(status){
        var _this = this;
        if(status == "kind"){
            if(this.ViewIsHide(this._ShowsClipView)){
                if(this.GetClipViewVisibility(this._KindsClipView) == "HIDDEN"){
                    this.RadioKindsViewIsHide();
                    this.ScrollView(this._RadioView,695,50,null);
                    this.ScrollView(this._KindsView,0,0,null);
                }else{
                    this.ScrollView(this._RadioView,432,50,null);
                    this.ScrollView(this._KindsView,400,0,function(){
                        _this.RadioKindsViewIsHide();
                    });
                }
                

            }else{
                this.RadioShowsViewIsHide(true);
                 this._ShowsView.ResetLayoutParams({x:-414,y:0,width:414,height:540});
                this.RadioKindsViewIsHide(false);
                this._KindsView.ResetLayoutParams({x:0,y:0,width:524,height:540});
                this.ScrollView(this._RadioView,695,50,null);
            }

        }else if(status == "show"){
            if(this.ViewIsHide(this._KindsClipView)){

                 if(this.GetClipViewVisibility(this._ShowsClipView) == "HIDDEN"){
                     this.RadioShowsViewIsHide();
                    this.ScrollView(this._RadioView,226,50,null);
                    this.ScrollView(this._ShowsView,0,0,null);
                }else{
                    this.ScrollView(this._RadioView,432,50,null);
                    this.ScrollView(this._ShowsView,-414,0,function(){
                        _this.RadioShowsViewIsHide();
                    });
                }
            }else{
                this.RadioShowsViewIsHide(false);
                this._ShowsView.ResetLayoutParams({x:0,y:0,width:414,height:540});
                this.RadioKindsViewIsHide(true);
                this._KindsView.ResetLayoutParams({x:414,y:0,width:524,height:540});
                this.ScrollView(this._RadioView,226,50,null);
            }

        }

    };

    //移动的函数
    MainScene.prototype.ScrollView = function(view,x,y,callback){

        var animationEndCallback =  Forge.AnimationHelper.MoveViewTo(view, {x: x, y: y}, 400, null);
        animationEndCallback.SetAnimationListener(new AnimationListener( null,function(){
            if(callback != null){
                callback();
            }
        }))
    };

    // var exitKeyCnt = 0;
    MainScene.prototype.OnJavaKey = function(keycode,action){
        if(action == 0){
            return true;
        }
        if(keycode == 4){
            if(!this.ViewIsHide(this._ShowsClipView)){
                this.ControlAllClipViewStatus("show");
                this._RadioOperBlock.Focus();
                return false;
            }
            if(!this.ViewIsHide(this._KindsClipView)){
                this.ControlAllClipViewStatus("kind");
                this._RadioOperBlock.Focus();
                return false;
            }
            // exitKeyCnt ++;
            // if (exitKeyCnt == 2) {
            //     if (typeof jContentShellJBridge.backToHomepage != 'undefined') {
            //         exitKeyCnt = 0;
            //         jContentShellJBridge.backToHomepage();
            //     }
            // }
            return true;
        }

        return true;
    };

    MainScene.prototype.OnKeyDown = function(ev) {
        var key_used = MainScene.__super__.OnKeyDown.call(this, ev);
        return false;
    };

    MainScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");
        console.log("HidePage : ",this);
        //this._MainView.ClearViews();
    };

    MainScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");
        console.log("ShowPage : ",this);
        
    };

    MainScene.prototype.ViewIsExist = function(view){
        if( typeof view != "undefined" &&  view != null){
            return true;
        }
        return false;
    };

    MainScene.prototype.AddHistory =function(data){
        function del(arr,index){
           if(isNaN(index)||index>=arr.length){
                return arr;
            }
            var a_arr = [];
            for(var i=0;i<arr.length;i++){
                if(i != index){
                    a_arr.push(arr[i]);
                }
            }
            return a_arr;

        }

        var history = this._Data[1].list;
        var len = history.length;
        for(var i=0;i<len;i++){
            if(history[i].content_id == data.content_id){
                    history = del(history,i);
                    break;
            }
        }
        history.unshift(data);
        len = history.length;
        for(var j=len;j>10;j--){
            history.pop();
        }

        this._Data[1].list =  history;
        this._DataMain.SaveHistoryData(history);
        
    };
    MainScene.prototype.AddHistoryItemData = function(data){
        if(this._HistoryData == null || this._HistoryData.length == 0)return;
        function del(arr,index){
            if(isNaN(index)||index>=arr.length){
                return arr;
            }
            var a_arr = [];
            for(var i=0;i<arr.length;i++){
                if(i != index){
                    a_arr.push(arr[i]);
                }
            }
            return a_arr;

        }
        var len = this._HistoryData.length;
        for(var i=0;i<len;i++){
            if(this._HistoryData[i].id == data.id){
                this._HistoryData = del(this._HistoryData,i);
                break;
            }
        }
        this._HistoryData.unshift(data);
        len = history.length;
        for(var j=this._HistoryData;j>10;j--){
            this._HistoryData.pop();
        }

    };

    MainScene.prototype.AddCollection = function(data){
        var collection = this._Data[0].list;
        var len = collection.length;
        for (var i = 0; i < len; i++) {
           if(collection[i].content_id == data.content_id){
                return
           }
        };

        collection.push(data);
        this._Data[0].list =  collection;
        this._DataMain.SaveCollectionData(collection);

    };

    MainScene.prototype.IsCollection = function(item){
        var collection = this._Data[0].list;
        var len = collection.length;
        for (var i = 0; i < len; i++) {
           if(collection[i].content_id == item.content_id){
                return true
           }
        }
        return false;
    };

    MainScene.prototype.RemoveItemWithCollection = function(item){
        function del(arr,index){
           if(isNaN(index)||index>=arr.length){
                return arr;
            }
            var a_arr = [];
            for(var i=0;i<arr.length;i++){
                if(i != index){
                    a_arr.push(arr[i]);
                }
            }
            return a_arr;
        }

        var collection = this._Data[0].list;
        var len = collection.length;
        for(var i=0;i<len;i++){
            if(collection[i].id == item.id){
                    collection = del(collection,i);
                    break;
            }
        }
    
        this._Data[0].list =  collection;
        this._DataMain.SaveCollectionData(collection);  
     };

    MainScene.prototype.AudioToast = function(content){
        _this._PageUtils.Toast(content,2000);
    }


    return MainScene;
}(WebappTemplate.WebappTemplatePage));