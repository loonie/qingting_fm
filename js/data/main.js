WebappTemplate.DataMain = (function(){
    var DataMain = function(){
        this.Main_data = {};
        this.PageSize = 50;
        this.DataManager = new jQWebapp.DataManager("QingTingFM");
        this.Main_data.ERROR_LOGO = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFkAAAAnCAYAAABkI+QeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MjMxQTI5NTQ1OTU2MTFFNjgyMDNDMTk3M0Y3QzM0NUIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MjMxQTI5NTU1OTU2MTFFNjgyMDNDMTk3M0Y3QzM0NUIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyMzFBMjk1MjU5NTYxMUU2ODIwM0MxOTczRjdDMzQ1QiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyMzFBMjk1MzU5NTYxMUU2ODIwM0MxOTczRjdDMzQ1QiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PtijDjcAABaxSURBVHja3FsHdFTltp4zLWXSJ72RhEAKIYREqaEooEEBpYgK6nqIy2Xjwr2CCFdF1GtZosvC06ciuuAKEpcUBUXwPkqo0gMkkBBCCpmUSTKZ3s6c9+2T/wxDyCB6XfctGdZZMznlL9+/97e/vf8DJ1fIZTfBh/NzXvhP9+3hPdf0qZTdJB9BEDg/AAgcx/2RgHPX65eTi50JgkcQrpz781syB+uhSXh6uSbHB+h6JywBft3Fwkf4levUp8xPn9cA/acHmSadnpbOjSwemYnfkQ67w2az2wx6vb7j0MFDVh8L5vBPLufkggQ6TnAefPzYq1wCm+4j++0J7KBBg+T9+/ePVKlVUbg3Ev3LysrKLjbUNxjwNy8IYj/Cnx1kjnEggbUQE9PQSZvNZrJare37yvbpf/jxB53JZGpav359Gy7xPhYOtAVPWnqa8r4Z9yVfqLlgam1pNQ4bNiy58lxl6/bt261ok2PWKD0ne/KpJ0MC1AGJ6enpCY899lisUqmMUalUWvQtAjlr1qwy9HUUi2LF8rpvCpABAkcWU1FRcVdOTs6tvd0EkDsuX77cvnHjxqbGxsbajz/++DIDTrh36r0ZmzZueri5ufms0WjsgGWOqq6uPoTvA7hOnsAvW7YsIigoKL24uDilqKgoVq1Wx2CN1D37qa+vNw4fPvxoU1NTNf5sx+G6GUAmupAT0FFRUaEvvvTisCmTp2RlZGRE+rsfFm7p6urq3Lp1a+PJEycr7Q67/c033xy/fPnyzoaGBsPKlSuzFy9eXFNzsaZlzn/NiRwyZEifvLy8aACr9adiWlpa7N9++63+hRdfaOjs6NThVBMOg7SQNwUngzDl5Nr4UxMdHZ0SHhEeP/XeqZGFhYUhcOvghISEoLi4uKDAwECF77MOh8PW2tqqmzdv3lG0Ew4rDPhw5Ye6+fPn5897Zl4qrDe0R18CALXpdDp7bW2tvby83IbFsuK3taOjQ49bukRwOZkRY3LeLJzstWaJOvCtwhGMIwhcGQTODFYoFMFw7yAsQODQoUMDs7KyAlJSUgLg+iF79+5tP3DgQOOqVauGAdSAd9555wiuyXJzc5Pr6upcoA4XaMC5a9cuJ8DkeQ/vRhyw8Txvc7lcRCdOdtgpHABgOwCWgp44vJtFJzOLEaWTkwIO5mgBCAo6cI4OJThXefHiRYU6QB2oUqqCwsPDQ5xOp3vq1KlhwfiQNMvMzIxesWLFeYDbigAqx/M87rGhPQLSzShAOuhvNx67cp4TpdtVyRB3k2R8vkmBZNGS1OJiY2OVmhCNBhYYNHPmzLApU6bE41xSampqGgC0LFq0aF98fHzY8BHDExfMX6Bbs2ZNH1h5Yk1NjR5BrHnHzh1NpRtK9Xa73Yr7zW1tbXZpYal9LK6sZwLim/xc15KZLpTdyEJI997o/b/yHPd7Mz1mRR64PJecnBwJSRb10ksvJYCP+yB4JYE2AsUb8AGAxs8//7xhQ+kG+7Rp01RD+CEGk9mkxzkbSbL8/PxYqIz4sWPHFrz+j9dlCJjNnZ2dTRs2bGhEoGszGAwdsHgb4OR6jFi4Ojm5FhBOGjwChgfuImtvb+f8TlCgFsVvT9/MvkLNhRq5lPF4G7ySQQk9nydQYFFCl7FLBneWd9sGy84wPEocxGRAfPiqJKLXjAsczM19bG7iB+9/UAQu7osjzPc6JJxp7T/XGr7b8l3zoUOH2hiXGpBA3JackpyOOW+GnCN1oJw0aVK/kpKSdNBJVGJi4lXtgJPdALxhy5YtVdDLJ4gyMB/+xkEWZBSthXXr1g1QKBWOWQ/OqkOjPMDzMEu5Ju384osv4rHypCNPsMl7erQqpqs+gIup6R133qF4ZfkrBUeOHmmd/5f5OrIwSoX9ZmK9fGbMmKEICQ0JPH/uvPNsxVm+vq5+MPh2El0jTkXktx0/cdz6ySefdKpVaisCmd1oMgqaYI2lqqqqjvTxhQsX7unTp0/fhMSEXfo2/QWmkQmcUICvHTFsRHrJXSWxY8eM1ZBSAYWLOhnjbhxy65Bt+NlJgqU3kJW9AcyM3wNXGxMWFkZ6zyjKE0Hm6Ne/n3z27NlacFMoUljB7XbLgjXBwv3331+E6Jy7/JXlrtqLtbbIyEgXtQJLkiHYtG3etNkiiCFXzP1lYj/oY+TIkXJo0Yno5xiumnGO7nNhsVKRKBTiPiX6cGF13V2GrgrIs1ilQhmHiatnTJ/hgYLQoI8IHAEHDx78edSoUeeys7Mrhw4b6kHgSjB2GZXEoeDXzpiYGDkkVwH6i6fJYg72tLQ0Atlb08DakjqhlNqJ8ZNRORsbGg2lDaVNpd+UakBB2sSkRC1ADsH8PWfPniVvCGAL0utH2ZMimKWKZg/OMuFwwYhJEpkJownjJyiXPL9kFCaXRYOUGsIk1QBD+OuCv07Abw8OEVJ8K3766af/3fjtxkpaaaqtEIeRp9Bzy15a5np+8fNde/bsof5Jl1rpQYCleW7RcykWi4UPCQkJR39qLKQBiUJSREREHPqSY2EizGazBRKsftu2bR4ASOCEwN27tmzeUoW2KDFQs/k4Kysrp2i12njItksfffSRBZLOhXspFTeCrwVmg4JPUCPv5Znxke61gG7acagZsCqmMMjqXRQAhWsrnV664BjAAqxXuWTJkmy4WfyYMWPySeJ8/fXXzUhNq19++eUzcEM5rDnZZrUlQMwH0nMIKsJ7772XDAuJg+tWw/XswNbFLFdm6DS0gr8uix7BQV6hn9FjRgc9+7dnB8IyogcOHJiPZ4w7duxoPnz48Gnk/rWwWHWIJkQLEIJXf7F6+Phx4yPA3Yc1Gk0HTR6BTbt///5RAF0HGiBATeQF9A2gzQwkUb5hFO4XXnghFOOfo9fr3WjnGOYl3W+jFBjA3QMayMRRBno5jzbafWsWIszdxki/wHmCgv2NLIjrpscr8q13uiA8MHH5N6XfTIRbDQSonSTO4XLqhx56KBnu3zcnJycJ1rQb3NcM66bsJliK7gAxCPdqEUx0kDoGWIYV9xCdUNMEuEscHoYDKw1EP5NhSX3xXDslE/gdjH7S5s6d2wecvOurr746D3rQU40APBh6/vz5Ntxng2sTfdnwnBPWzevb9TRBM6sVmFm9gGeg8KIbo1d4g5LGitSZ9DNp3jZGg2SJ1BbVjLirSqFcr3Vk0hICA/aGNgjkkhXTLQAxFQDnI1qehY48tm/fPhOspR0y5uTWbVsvT5w4MXvY8GHJR48eHQ858zBWfDomOw3f06A9B8AVg2sv1Rbj3EQokmmQONPhzjMg7mPEyTOHBA+moP2ce+655zS84gQWxwltqsvLyzsNC+Mm3DEhH/dRNOffeuutaFht6Ip3VjSDYswsuJjwnJUoCeB5xASEk9ngrkRHEsDe+qZYmbPbOCkJIy+jU3Q/PYsFFjAHB1JsBzI6gRkdub7gc3THbhxiUcpzzXXRwokymLrqPfDB/YNhfa5jx45Z8duMSdHg7QC07cD+A4q7Jt6ViIbC4Ha1I4tHdsGV1S6nS6y7QuYkYjHioCdrYb12BAY3VRNhIbLS0lIe9wSQ27KIH4y2XRWVFbbOjk4zJs273C4rXLkVHmTAfaRlQ8jSsOiZAN5x8MBBKwOYDgEL42HBy8ulIjtxDFiBxRgm+yhIe1VNt3KhoEbfnNVm5eb9Zd4B9F0DahOBBy0JEZERaoUcAQZBF8HPJcUR+mBcHCkM9KGg9kA3DnEREdBJhRHQUnKi7KH9xJGS65DroRFe6VLSQOywUjuBiYHIwdHt32/9Xu92uQk4BYFZMLggsF+/flGrV6/WIVh1ATi7cOXjFLmL40QLITXo2w+0rQf8y+NvJyjKjZRXzsammDVrVtrp06fb4B1mpmmpLRm4XAQ5KipKmjj9LeBZDovM9XRhBEteokXEIQ8DzANak1ktVmH156v7YRxJ0MYVgwYNSnzttdeK8IwWtwfA4Cyvv/56xdq1a+sBrnrevHnxGFcS5kjXA0kzI5jWYKHOXKq9ZGXJicThfmoX3Td4fNyNACF+dd19191BiOLFcLEQ4lwme2RYeS0GHHz8+PECAEVg8XSeNG9tbW0VAmo5BnvN/psEPt1Hv2nBrFar2PfSpUsz0G8clEc5Flx49tlnoxDoimih8XQQJhm0+LnFCVMmTxmM4JmH/log307DUyiYeQ4eOpg8uGDwICyQgGtUm1BDC0f86+d/FSQlJWVigRwL/rbg7Jov11zGfJIx5v5lZWUpmIeC6hXbfthmdTqcfMnEkrS33357wIIFCy7ivjBcj4Qnmb/7/juLxWxxFRYWRmJx7iwoKMgqLCrc1tbaJu6MwJpFmrnhApEUEHQtOnJ/ucPpoLIfyTGBAgYG1QoQOQxABYsgXuQh/JUQ9wqArCGKYFbo6i1U+OzDyerq6pwAQL18+fI7qGsExAxMIvrUqVPnABaBQRJKiTEpoXaCAWwCWSToKwD9NLKAZ0ewDoGlUcmSPIcimwLGoKYiEBYvkqjRYrJQwOzEcwJUkufTTz81bN++vQFgN2OxRQ+ELg44XX76dnhP388++6wBKfX+3bt3d2K+LkmBrP96/bAH7n8gDXEtHyAfw4TMhAGN/4ZBJqskrVq2t8yyds3aQ3QqNDQ0BGBoyPWYyhCDKFkjeQNSVSN73MU2MTlBEPzV0bzBQhutlUOhyBcuWlgNjqTkJ+m2226jGsIp6PadRCOwyPCTJ0+Oe/XVV1vefffdaqYsunU4k1rZWdl0vpXYAslNFIC5HUHbOG7cuLMYs9GH4ymukM7nFy5cWA3wO3wyOHfT5SZ+8+bNtY8++uggXG+AwmlnRXm7CKQgc0HClhPIQYFB0RRPYDQ2Knn6LRBxvdRnJL5mF+179u4ZUDyy+FZ394f3FTx0X0BAQCDcumvOnDl7kd+3iAOGRvaxXsG3G0EqS+CTEJ+gghXZ3n/v/cM0xuiYaH7AgAFyZG6S+O8kHoT18lAzbibF2lkiI0oyxv8C08LOhIQEAR4mwKqJj21MwlmZJXoXBzxMtZp2/N2B0bhY0YkH59oAMgU8BUDuEnU01x0fqM/4+HgT26km3gtk2DqvAVkSiRarxasBuwNwdw2ALne0dzhHjByhHT1q9HBY0QVIrxbK6kTr9QEZg5Ft2LCh6IMPPhi28+edPyC4WLw8z/qBtXp8PQWL5WGBkmOAkyV1IsmJwDWqjNnYwM1QMiqScOFh4TxbQAuuW3oUpuhweXiP49Yht1LxnmKLB2C70J4VgJiYwvC+m8Foy8UAlryCAiznE2ClWrLXa0CNvkUwztforgIZasFC6TEysRgkBVVYdYGCGyYUPHPmzDQA7YJ1umK0MVoM0rlp06ZGZGoGH1Hvjernzp3jkJjEwsUTIYNi2T02tmBmLIrq6aeejn7jjTcuQo2Q3uXunnR3BCgoBulvE/hUgwXKQ8p8CoEluFnXTLxpZ/0I/sS/b1pLgUfyF7Y91TN54PxsAHC+1cDrJBo39D6H0rcBJB9N4LmTDz/88KBp06bF024B+RZ4LBHuL0e6XUHlPHJfAmnnzp3DqcRHLsisRrRC8lJEXx6BRIOExIKFUrE8XwT5lyO/1FeeqyyfP39+7hNPPJGM+4IfeOCBHGSTWdDk1pX/vfJCWnpaKBa2KDc3NzApMSnorTffqmVSkGfOwvVST+YkmoBME/y8VcTKE/+5j9KrHCCKkHK6hg4dWlZSUtJWXFysfeSRR3LAPy5QQhMyPn35qXIKBqaw8DAxMsOSddXV1SZYHU9FId9JIdNzAiSSSWops6S5EWddbrzsLCos2oUssXnChAkxAHhAeXl555dfftkM5dICoE0A1kRkDy4uRFsOyEZJIzvYNj/9FhDheYYc7at5xNd7uncqpGrfv/NenPBbNhB+rQon8REJeSsmVIkjGFyYCkN2wKUbca0TgcYMqWaAFSdTsPvwww/bKioqWmCJzqvqxzBqu83uSExMjJk+fXqMjysJLOBx4GMLAD0DDRwBC848dPiQYdWqVY24txO8aTpz5owJSqbmzpI7b4FzdVFNmKgis19mLMWW3AG5kbRJivQ6HPwfqwnRqHBed+nSJTNLeAUf+v9//fQMfDJWmKcU0QYaoFSSKjzm1NRU/sSJE/3H3jb2OHSsA4AH7NixIxugp0J7ehhVeNuh6B8WFhZH4Pjs3F55AbC7YmVHdkWgUl3YCW+wgIKim3RN8bNnzT720f98VEcgmy1mDyuruivOVtyJRY6lJkmnPrfoueilS5aOpaAGBbBn/Pjx5Swz5H0CmeyPsMg/BGRx/bsHJUoPivaYkBi9AZoSrj86KjJKD3cWwdq9Z7e7vq6eDwwK9LDtIrJQWWtrK4HifPDBB11YHKX0ChPrQ8r0pJo19eFxOV0U8R2DBw/OBI+T1rwAwOLonnG3j0tAW2nr169vHD5i+FZYcKzZZNa4ebcKyoHap0VyNzY2UqYVCD52gvKCkWKHnj572lhTXWP5TQQh81N9+61PXgMyboGKUIwePTqA0lC9Xu+Ki4uT47ecePWZZ57pQ0tQVVWlzMrO0tCLIXMfnXsKysDM9GbPTgTw+jgkDSRMeSmro/R81OhRioz0jAAsBo/+VLTjhD4UsOpQBFgtqMICCop96smnbln7z7V1UCfcunXrBs+YPiNo2cvLjlfVVbXjGYPEBSwZouoa8bKDd/P8N6Xf5KOtkUjDt69YsaIayqQ3dSH4AdQveJJXCr3snv4qyPTsrNmz8l584cXxHvYBJUSwd7ySQQmBP/74Y5VOp+OhPGiPKwwCXwUJR6LdKFqmIPNkZWUFQnaNRbqbSQrk8ccfp1KmhV0XaGN229ZteWh7PO0bkhDA71AE2XxkdgNIvcDty59++uk02lL6+9K/VyEgE+VY7rvvvr6Tp0zOwGNOtl94VXBCDlGbnp6+m9J7DD+KXmhB4CZvaAkLDVPQVhX42y5tzPqCCO/Q4LpSoVRwvQU+rVYbRN/wWkVvCxAeEa5ghSgVC/LeZOvKzgj0Zd/MvqmwphyH3UHFeCXAcRPW6FyBaG6ElNPBpQ0ZGRmy9Iz0JASmLrCCjoIVSy8F5PdKWPAgm90WhyBkPXniJGVilKZSFmWnEuKkyZNSAXYOng2G1SmR3dELIgE0QdBPG7yleXDhYBXVQY78csSEMdAGgSM7JzslPi6eahGBoga+YlKiPqYy6f79+89RNlYysSQO7ffD82C3rsY+aX0seQPyckEpQVAyTZjfZbRhpufjE+K5W265JdfYZUzB862YYz3a7PCm6OgpMzMzDV7Zr2xfWQfwoV2eNjzvlnLX5JTk8PyB+QV43g26o/pJC67bxDp0j+0ncXeWFcxVvbiNg6WpRA9BbP+MXk2ycrQjw3mTATofzu5xsm0nytg8jJOv14+HBS6T9OYPLQ7RAGs3hLWr6MW9eTY2I/sdwvbibGzcatYvz7apnD5jVrExK9g1q0/xn7BRsmdVYh/dGaanxws1VATTsFqNSSqGeV8JYLsjdCNt0yjpJege2p2u0n6WmE5K75/Rue5NJVEDX3k3rbvCJxc3I+k1Ju6aqsXV/QjeFJWuuml7h73P0f0aAecdhUKgEq3Q6+6wwPqTlIWc7b5L+3AcG3f3XK6AJI1ZmhMvzck7L0F81ruv57v/53NdzuYssMxUds0Lh/4ypOulrj5v/wjsjSApGfB7r+/1G+3nRp/5PWP3uzfnMyd/133egPIbNP39Z5bfJ3b8tyP8G/38YZnXbxj7H/r5PwEGAFgQ2lXXtqVWAAAAAElFTkSuQmCC";

    };

    DataMain.prototype.GetMainPage = function(url_,success_cb,error_cb){
        var _this = this;
        this.DataManager.Ajax.Get({
            url:url_ == null ? "http://qingting.fm/category/domestic/54" : url_,
            success:function(result){
                console.log("SUCCESS");

                 var doc = document.implementation.createHTMLDocument("");
                doc.documentElement.innerHTML = result;
                var data = [];
                var kinds = doc.querySelectorAll("div.menu li");
                for(var i=0,len=kinds.length;i<len;i++){
                    var kind_obj = {};
                    kind_obj.url = kinds[i].querySelector("a").getAttribute("href");
                    kind_obj.title = kinds[i].innerText;
                    kind_list = null;
                    data.push(kind_obj);

                }

                data[0].list = [];
                var list = doc.querySelectorAll("ul#channels li a.channel");
                for(i=0,len = list.length;i<len;i++){
                    var list_obj = {};
                    list_obj.title = list[i].innerText;
                    list_obj.url = list[i].getAttribute("href");
                    var arr = list_obj.url.split("/");
                    list_obj.id =  arr[arr.length-1];
                    data[0].list.push(list_obj); 

                }
                success_cb(data);

                // get title
                
            },

            error:function(data){
                
                error_cb(data);
            }

        })
    };

    DataMain.prototype.GetRadioKindsData = function(success_cb,error_cb){
        var _this = this;
        this.DataManager.Ajax.Get({
            url:"http://www.qingting.fm/",
            success:function(result){

                var doc = document.implementation.createHTMLDocument("");
                doc.documentElement.innerHTML = result;
                var data = [];

                var kinds = doc.querySelectorAll("a");
                for(var i=0,len=kinds.length;i<len;i++){
                    var url = kinds[i].getAttribute("href");
                    if(url != null && url.indexOf("categories") == 1){
                        var kind_obj ={};
                        kind_obj.title = kinds[i].querySelector("h5").innerText;
                        kind_obj.url = url;
                        var arr = url.split("/"); 
                        kind_obj.id = arr[arr.length-1];   
                        data.push(kind_obj);              

                    }
                }
                for(i=0;i<2;i++){
                    var temp = data.pop();
                    data.unshift(temp);
                }
                data.pop();
                success_cb(data);
            },
            error:function(result){
                error_cb();
            }
        })
    };

    DataMain.prototype.GetRadioData = function(params,success_cb,error_cb){
        var _this = this;
        var url = "http://rapi.qingting.fm/categories/"+params.id+"/channels?with_total=true&page="+params.page+"&pagesize="+this.PageSize;
        this.DataManager.Ajax.Get({
            url:url,
            timeout:3000,
            success:function(result){
                result = JSON.parse(result);
                if(result.Success == 'ok'){
                    success_cb(result.Data);
                }else{
                    error_cb();
                }
            },
            error:function(result){
                error_cb();
            }

        })

    };

    DataMain.prototype.GetRadioShowData = function(id,success_cb,error_cb){

        var _this = this;
        // var time = (new Date()).getTime();
        // var url = "http://i.qingting.fm/qtapi/channellives/"+id+"/programs/day/1/2/3/4/5/6/7?_="+time;
        var time = (new Date()).GetCurrentDate();
        var url = "http://i.qingting.fm/wapi/channels/"+id+"/programs/date/"+time;
        this.DataManager.Ajax.Get({
            url:url,
            timeout:5000,
            success:function(result){
                result = JSON.parse(result);
                if(typeof result == "object" && typeof result.data == "object" ){
                    success_cb(result.data)
                }else{
                    _this.GetRadioShowData(id,success_cb,error_cb);
                }
            },
            error:function(result){
               var show  =[]
                success_cb(show)
            }
        })
    };

    DataMain.prototype.GetCollectionData = function(){
       return this.DataManager.GetCollection();
    };

    DataMain.prototype.GetHistoryData = function(){
        return this.DataManager.GetHistory();
    };

    DataMain.prototype.GetUserSaveData = function(data){

          var history_obj = {
            title:"最近收听",
            list:this.GetHistoryData()
        }

        data.unshift(history_obj);

        var collection_obj = {
            title:"收藏电台",
            list:this.GetCollectionData()
        }
        data.unshift(collection_obj);

      
        return data;
    };

    DataMain.prototype.SaveHistoryData = function(data){
        this.DataManager.SaveHistory(data);
    };
    DataMain.prototype.SaveCollectionData = function(data){
        this.DataManager.SaveCollection(data);
    }
    
    return DataMain;

})();
Date.prototype.GetCurrentDate = function(){
    // var curr = new Date();
    var year = this.getFullYear();
    var mouth = this.getMonth()+1;
    if(mouth<10){
        mouth = "0"+ mouth;
    }
    var date = this.getDate();
    return year.toString() + mouth + date;
}

Date.prototype.GetCurrentTime = function(){
    var curr_date = new Date()

    var  curr_hour = curr_date.getHours();
    var curr_minute = curr_date.getMinutes();
    var curr_sceond = curr_date.getSeconds();

    var curr_time = curr_hour+":"+curr_minute+":"+curr_sceond;
    return curr_time;

}
