var audioControl = {
	audio:new Audio(),
	// audio: document.getElementById("audio"),
	isPlayBeforeShow:false,
	count:0,
	canPlayM3U8:true,
	radioUrlId:null,
	init:function(toast){

		this._Toast = toast;
		this._Timer = null;
	},
	play:function(src){

		 console.log("scr = " +src);
		 this.audio.src = src;
		 // audioControl.audio.load();
		 if(this._Timer != null)clearTimeout(this._Timer);
		 this.audio.play();
		 console.log("playing");
		
	},
	dealRadioLiveSrc:function(id){
		this.isPlayBeforeShow = false;
		this.radioUrlId = id;
		var src ="";
		if(this.audio.canPlayType("audio/mp4; codecs=\"mp4a.40.29\"") == "probably" && this.canPlayM3U8 == true){
			// src = "http://hls.qingting.fm/live/"+id+".m3u8?format=mpegts";
			src = "http://ls.qingting.fm/live/"+id+"/24k.m3u8"
		}else{
			// src = "http://http.qingting.fm/"+id+".mp3";
			src = "http://lhttp.qingting.fm/live/"+id+"/64k.mp3"
		}

		if(typeof  jContentShellJBridge != "undefined" && jContentShellJBridge.getDeviceInfo ){
			var device_info =JSON.parse(jContentShellJBridge.getDeviceInfo());
			if(device_info.MODEL.indexOf("BAOFENG") >=0 ){
				src = "http://lhttp.qingting.fm/live/"+id+"/64k.mp3"
			}
			if(device_info.MODEL.indexOf("Allwinner-INPHIC_I9H") >=0 ){
				src = "http://lhttp.qingting.fm/live/"+id+"/64k.mp3"
			}
			if(device_info.MODEL.indexOf("Letv New C1S-MBX") >=0){
				src = "http://lhttp.qingting.fm/live/"+id+"/64k.mp3"
			}
			if(device_info.MODEL.indexOf("Judou J1 Plus") >= 0){
				src = "http://lhttp.qingting.fm/live/"+id+"/64k.mp3"
			}
			if(device_info.MODEL.indexOf("Allwinner") >= 0 ){
				src = "http://ls.qingting.fm/live/"+id+"/24k.m3u8"
			}
			if(device_info.MODEL.indexOf('TestLauncher') >=0){
				src = "http://lhttp.qingting.fm/live/"+id+"/64k.mp3"
			}			
		}

		//http://lcache.qingting.fm/cache/20170320/276/276_20170320_060000_070000_24_0.aac
		//http://lcache.qingting.fm/cache/20170320/386/386_20170320_120000_130000_24_0.acc
		return src;
	},
	delRadioBeforeShowSrc:function(src_params){
		var src ="";
		this.isPlayBeforeShow = true;
		// if(this.audio.canPlayType("audio/mp4; codecs=\"mp4a.40.29\"") == "probably"){
		// 	src = "http://hls.qd.qingting.fm/cache/"+src_params;
		// }else{
		// 	src = "http://cacheshare.qingting.fm/cacheshare/"+src_params+".m4a";
		// }
		// if(typeof  jContentShellJBridge != "undefined" && jContentShellJBridge.getDeviceInfo ){
        //     var device_info =JSON.parse(jContentShellJBridge.getDeviceInfo());
        //     if(device_info.MODEL.indexOf("MagicBox_M16C") >=0 ){
        //        	src = "http://cacheshare.qingting.fm/cacheshare/"+src_params+".m4a";
        //     }
		// 	if(device_info.MODEL.indexOf("Allwinner") >= 0) {
		// 		src = "http://hls.qd.qingting.fm/cache/"+src_params;
		// 	}
        // }  
		src = "http://lcache.qingting.fm/cache/"+src_params;
		return src;
	}
}

//客户端开始请求数据


// request error
audioControl.audio.addEventListener("error",function(e){
	switch (audioControl.audio.error.code){
		case 1:
			console.log("error code = "+audioControl.audio.error.code+",用户终止");
			break;
		case 2:
			console.log("error code = "+audioControl.audio.error.code+",网络错误");
			break;
		case 3:
			console.log("error code = "+audioControl.audio.error.code+",解码错误");
			break;
		case 4:
			console.log("error code = "+audioControl.audio.error.code+",URL无效");
			break;
		default:
			console.log("error code = "+audioControl.audio.error.code);
			break;
	}
	if(audioControl.audio.error.code == 4 && audioControl.audio.src.indexOf("m3u8")>0 && audioControl.canPlayM3U8 == true ){
		audioControl.canPlayM3U8 = false;
		var src = "http://http.qingting.fm/"+audioControl.radioUrlId+".mp3";
		audioControl.audio.src = src;
		audioControl.audio.load();
		audioControl.audio.play();
	}else if(audioControl.audio.src.indexOf("mp3")>0 && audioControl.count<3){
		console.log("error  reload");
		audioControl.audio.load();
		audioControl.audio.play();
		audioControl.count++;
	
	}else{
		// 提示 该节目暂时无法回放
		audioControl.count = 0;

		if(audioControl.isPlayBeforeShow){
			var title = "该节目暂时无法回放"
		}else{
			var title = "该电台暂时无法播放"
		}


		audioControl._Toast(title);
	}
});

audioControl.audio.addEventListener("loadstart",function(){
	console.log("loadstart");
	audioControl._Toast("节目加载中");

});

audioControl.audio.addEventListener("pause",function(){
	console.log("pause");
});

// play end
audioControl.audio.addEventListener("ended", function () {
	if(audioControl.isPlayBeforeShow){
		var src = audioControl.audio.src,radioId=null;
		if(src.indexOf("m4a")>0){
			var arr = src.split("/");
			radioId = (arr[arr.length-1]).split("-")[0];
		}else{
			var arr = (src.split("?")[0]).split("/");
			radioId = (arr[arr.length-1]).split(".")[0]
		}
		src = audioControl.dealRadioLiveSrc(radioId);
		audioControl.audio.src = src;
		audioControl.beforeSHowEndPlayLive();
		audioControl._Toast("播放结束,切换至直播");
	}
	console.log("ended");
	if(audioControl.audio.src.indexOf("mp3") >= 0){
		console.log("reload");
		audioControl.audio.load();
		audioControl.audio.play();
	}
});

audioControl.audio.addEventListener("canplay",function(){
	//audioControl.canplaythrough_cb();
	console.log("canplay");
});
//可以播放，歌曲全部加载完毕

audioControl.audio.addEventListener("canplaythrough",function(){
	//audioControl.canplaythrough_cb();
	console.log("canplaythrough");

		if(typeof  jContentShellJBridge != "undefined" && jContentShellJBridge.getDeviceInfo ){
			var device_info =JSON.parse(jContentShellJBridge.getDeviceInfo());
			if(device_info.MODEL.indexOf("MagicBox_M16C") >=0  || device_info.MODEL.indexOf("Xiaomi-MiBOX3_PRO") >=0  ){
				audioControl._Toast("音频资源加载中....");
				if(audioControl._Timer != null) clearTimeout(audioControl._Timer);
				audioControl._Timer = setTimeout(function(){
					if(this.audio.played.length == 0){
						if(audioControl.isPlayBeforeShow){
							var title = "该节目暂时无法回放"
						}else{
							var title = "该电台暂时无法播放"
						}
						this._Toast(title);
					}

				}.bind(audioControl),8000);
			}
	}	
});

//abort

// audioControl.audio.addEventListener("abort",function(){
// 	//audioControl.canplaythrough_cb();
// 	console.log("abort");
// });

// audioControl.audio.addEventListener("pause",function(){
// 	//audioControl.canplaythrough_cb();
// 	console.log("pause");
// });

// audioControl.audio.addEventListener("play",function(){
// 	//audioControl.canplaythrough_cb();
// 	console.log("play");
// });


//stalled

audioControl.audio.addEventListener("stalled",function(){
	//audioControl.canplaythrough_cb();	
	console.log("stalled");

});



