/**
 * Created by Administrator on 2016/1/7.
 */

// var ErtongHuaData = {};

WebappTemplate.Template = (function(){
    //__extends(ErtongHua, _super);


    var CONST_MATCH_PARENT_WIDTH = 1280;//"MatchParent"
    var CONST_MATCH_PARENT_HEIGHT = 720;//"MatchParent"
    function Template(activity, root_view, page_manager, webIndex) {
        //ErtongHua.__super__.constructor.call(this, "GlobalSearch");

        this._HostActivity = activity;
        this._HostActivity._Data = {};
        this._HostActivity._JsonStringSet = {};
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = root_view;
        this._PageManager = page_manager;
        this._PageUtils = this._HostActivity._PageUtils;
        this._DataMain = new WebappTemplate.DataMain();
        this._webIndex = webIndex;
        this.MainColor = "#F48B47";
        this.TextColor = "#ffffff";
        this.SetLoadingTexture();

        window.debug_template = this;

        if(typeof qcastTop != "undefined"
            && typeof qcastTop.iframe_page_cast_ != "undefined"
            && typeof qcastTop.iframe_page_cast_.fakeReferrerUrl != "undefined"){
            qcastTop.iframe_page_cast_.fakeReferrerUrl = "http://m.qingting.fm/categories/0";
        }



        this.Init();
    }

    Template.prototype.Init = function(){
        this._PageManager.SetBackMode(3,"电台直播");
        this._PageUtils.SetToastViewLayout({width:1280,height:720});
        var bg_image_texture = this._TextureManager.GetImage2("./images/bg.jpg", false,{width:1280,height:720});
        var bg_image_view = new LayoutView();
        bg_image_view.SetId("BackgroundView");
        bg_image_view.Init(new TextureSetting(bg_image_texture, null, null, false));
        this._MainView.AddView(bg_image_view, {x:0, y:0, width:1280, height:720});
        // playerInit();
        this._LayoutInit();

    };

    Template.prototype._LayoutInit = function(){
        //add main page 
        var main_view = new Forge.LayoutView();
        this._MainView.AddView(main_view, {x:0, y:0});
        this._MainScene = new WebappTemplate.MainScene(this, main_view);
        this._PageManager.PageRegister("MainScene", this._MainScene);
        this._MainScene.Hide();

        // //add content page
        // var content_view = new Forge.LayoutView();
        // this._MainView.AddView(content_view, {x:0, y:0});
        // this._ListScene = new WebappTemplate.ListScene(this, content_view);
        // this._PageManager.PageRegister("ListScene", this._ListScene);
        // this._ListScene.Hide();

        this._PageManager.LaunchTo("MainScene", null);

    };
    Template.prototype.SetLoadingTexture = function(){
        this._DefaultImageTexture = this._TextureManager.GetImage2("./images/default.png",true,{width:180,height:180},"RGBA_8888")
    }

    return Template;
}());